defmodule Termak.Types do
  @moduledoc ~S"""
  Collection of types used in this project
  """

  defmacro __using__(_opts) do
    quote do
      @type binaries :: list(binary())
    end
  end

end
# SPDX-License-Identifier: Apache-2.0
