defmodule Termak.Renderer do
  use Termak.Types
  @moduledoc ~S"""
  Renders EarmarkParser's AST to a string
  """

  @spec render(EarmarkParser.ast) :: binary() 
  def render(_ast) do
    ""
  end

end
# SPDX-License-Identifier: Apache-2.0
