defmodule Termak do
  use Termak.Types
  import Termak.Renderer, only: [render: 1]
  @moduledoc """
  This is the main namespace of `Termak` the Terminal Renderer for Markdown,
  written in Elixir.

  Here are some examples of how markdown is rendered

  # Default Renderer

      iex(0)> termak("Hello")
      "Hello"
  """

  @doc ~S"""
  The frontend, in goes markdown (or lines of markdown) out comes rendered text
  """
  @spec termak(binary()|binaries()) :: binary()
  def termak(str) do
    with {:ok, ast, _} <- EarmarkParser.as_ast(str) do
      render(ast)
    end
  end

end
# SPDX-License-Identifier: Apache-2.0
