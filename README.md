# Termak

[![pipeline](https://gitlab.com/lab42-hex-projects/termak/badges/main/pipeline.svg)](https://gitlab.com/lab42-hex-projects/termak/commits/main)
[![coverage](https://gitlab.com/lab42-hex-projects/termak/badges/main/coverage.svg)](https://gitlab.com/lab42-hex-projects/termak/commits/main)
[![Hex.pm](https://img.shields.io/hexpm/v/termak.svg)](https://hex.pm/packages/termak)
[![Hex.pm](https://img.shields.io/hexpm/dw/termak.svg)](https://hex.pm/packages/termak)
[![Hex.pm](https://img.shields.io/hexpm/dt/termak.svg)](https://hex.pm/packages/termak)

Render Markdown in the terminal

## Installation

If [available in Hex](https://hex.pm/docs/publish), the package can be installed
by adding `termak` to your list of dependencies in `mix.exs`:

```elixir
def deps do
  [
    {:termak, "~> 0.1.0"}
  ]
end
```

## Author

Copyright © 2023 Robert Dober
robert.dober@gmail.com

# LICENSE

Same as Elixir, which is Apache License v2.0. Please refer to [LICENSE](LICENSE) for details.
Documentation can be generated with [ExDoc](https://github.com/elixir-lang/ex_doc)
and published on [HexDocs](https://hexdocs.pm). Once published, the docs can
be found at <https://hexdocs.pm/termak>.

<!--SPDX-License-Identifier: Apache-2.0-->
